﻿using System;
using System.Collections.Generic;
using System.Text;
using MusicStore_Common.Entities;

namespace MusicStore_Interfaces
{
	public interface IUserRepository
	{

		/// <summary>
		/// verify if the credentials are valid
		/// </summary>
		/// <param name="username"></param>
		/// <param name="hashedPassword"></param>
		/// <returns></returns>
		User Authenticate(string username, string hashedPassword);

		User GetUserByUsername(string username);

		/// <summary>
		/// Add the new user to the collection.
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		User Add(User user);

	}
}
