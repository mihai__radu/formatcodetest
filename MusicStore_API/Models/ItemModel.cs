﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace MusicStore_API.Models
{

	public class ItemModel
	{

		public Guid Id { get; set; }

		[Required(ErrorMessage = "Name is required")]
		[MinLength(2, ErrorMessage = "Length must be at least 2 characters")]
		public string Name { get; set; }

		[Required]
		[MinLength(2, ErrorMessage = "Length must be at least 2 characters")]
		public string Producer { get; set; }

		[Required]
		[MinLength(2, ErrorMessage = "Length must be at least 2 characters")]
		public string Type { get; set; }

		[Required]
		public double Price { get; set; }

		[Required]
		[MinLength(2, ErrorMessage = "Length must be at least 2 characters")]
		public string ListingDate { get; set; }

	}

}
