﻿using System;
using System.Collections.Generic;
using System.Text;
using MusicStore_Common.DTOs;

namespace MusicStore_Interfaces
{
	public interface IMusicStoreService
	{

		/// <summary>
		/// Get the item by the given id.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		ItemDto GetById(Guid id);

		/// <summary>
		/// Get the collection of items.
		/// </summary>
		/// <returns></returns>
		IEnumerable<ItemDto> Get();

		/// <summary>
		/// Get the collection of items, in a paged format.
		/// </summary>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		IEnumerable<ItemDto> Get(int pageNumber, int pageSize);

		/// <summary>
		/// Add the new item to the collection.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		ItemDto Add(ItemDto item);

		/// <summary>
		/// Update the item animal with the new values.
		/// Throws KeyNotFoundException if the entity cannot be found.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		bool Update(ItemDto item);

		/// <summary>
		/// Remove the item with the given id from the collection.
		/// Throws KeyNotFoundException if the entity cannot be found.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		bool Remove(Guid id);

	}
}
