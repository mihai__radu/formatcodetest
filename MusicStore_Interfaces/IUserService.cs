﻿using System;
using System.Collections.Generic;
using System.Text;
using MusicStore_Common.Entities;

namespace MusicStore_Interfaces
{
	public interface IUserService
	{

		/// <summary>
		/// Authenticate a user
		/// </summary>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		string Authenticate(string username, string password);

		string RenewAccessToken(User user);

		User GetUserByUsername(string username);

	}
}
