﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MusicStore_Common.Entities;
using MusicStore_Interfaces;

namespace MusicStore_DataPersistence.Repositories
{
	public class MusicStoreRepository : IMusicStoreRepository
	{

		private List<Item> _items;

		public MusicStoreRepository()
		{
			_items = new List<Item>();
		}

		/// <summary>
		/// Add the new item to the collection.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public Item Add(Item item)
		{

			item.Id = Guid.NewGuid();
			_items.Add(item);
			return item;

		}

		/// <summary>
		/// Remove the item with the given id from the collection.
		/// Throws KeyNotFoundException if the item cannot be found.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public bool Remove(Guid id)
		{

			var foundItem = _items.FirstOrDefault(x => x.Id == id);
			if (foundItem == null)
				throw new KeyNotFoundException($"Item with given id does not exist");

			return _items.Remove(foundItem);

		}

		/// <summary>
		/// Get the collection of items.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<Item> Get()
		{
			return _items;
		}

		/// <summary>
		/// Get the collection of items, in a paged format.
		/// </summary>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public IEnumerable<Item> Get(int pageNumber, int pageSize)
		{

			return _items
				.Skip((pageNumber - 1) * pageSize)
				.Take(pageSize);

		}

		/// <summary>
		/// Get the item by the given id.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public Item GetById(Guid id)
		{
			return _items.FirstOrDefault(x => x.Id == id);
		}

		/// <summary>
		/// Update the existing item with the new values.
		/// Throws KeyNotFoundException if the item cannot be found.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool Update(Item item)
		{

			var foundItem = _items.FirstOrDefault(x => x.Id == item.Id);
			if (foundItem == null)
				throw new KeyNotFoundException($"Item with given id does not exist");

			foundItem.Name = item.Name;
			foundItem.Producer = item.Producer;
			foundItem.Type = item.Type;
			foundItem.Price = item.Price;
			foundItem.ListingDate = item.ListingDate;

			return true;

		}
	}
}
