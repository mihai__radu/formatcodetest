﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MusicStore_Common.Entities;
using MusicStore_Interfaces;

namespace MusicStore_DataPersistence.Repositories
{
	public class UserRepository : IUserRepository
	{
		private List<User> _users;

		public UserRepository()
		{
			_users = new List<User>();
		}

		/// <summary>
		/// Add the new user to the collection.
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		public User Add(User user)
		{

			if (GetUserByUsername(user.Username) != null)
				return null;

			user.Id = Guid.NewGuid();
			_users.Add(user);
			return user;

		}

		/// <summary>
		/// verify if the credentials are valid
		/// </summary>
		/// <param name="username"></param>
		/// <param name="hashedPassword"></param>
		/// <returns></returns>
		public User Authenticate(string username, string hashedPassword)
		{
			return _users.SingleOrDefault(x => x.Username == username && x.PasswordHash == hashedPassword);
		}

		public User GetUserByUsername(string username)
		{
			return _users.SingleOrDefault(x => x.Username == username);
		}
	}
}
