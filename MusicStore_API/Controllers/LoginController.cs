﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MusicStore_API.Middleware.Auth;
using MusicStore_API.Models;
using MusicStore_Common.Entities;
using MusicStore_Interfaces;

namespace MusicStore_API.Controllers
{

	/// <summary>
	/// Authentication for users
	/// </summary>
	[Route("login")]
	[ApiController]
	public class LoginController : ControllerBase
	{

		private readonly IUserService _userService;

		public LoginController(IUserService userService)
		{
			_userService = userService;
		}

		/// <summary>
		/// Authenticate a user
		/// </summary>
		[HttpPost]
		[AllowAnonymous]
		[Produces("application/json")]
		public IActionResult Login([FromBody] AuthenticationRequest login)
		{

			IActionResult response = Unauthorized();
			var token = _userService.Authenticate(login.Username, login.Password);
			if (token != null)
				response = Ok(token);

			return response;

		}

		/// <summary>
		/// Refresh the access token
		/// </summary>
		[HttpPost]
		[Route("refresh")]
		[Authorize(Policy = Policies.All)]
		[Produces("application/json")]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		[ProducesResponseType(StatusCodes.Status401Unauthorized)]
		[ProducesResponseType(StatusCodes.Status403Forbidden)]
		public IActionResult RefreshAccessToken()
		{

			IActionResult response = Unauthorized();
			var user = (User)HttpContext.Items["User"];
			var token = _userService.RenewAccessToken(user);
			if (token != null)
				response = Ok(token);

			return response;

		}

	}

}
