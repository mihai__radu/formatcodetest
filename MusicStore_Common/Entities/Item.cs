﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore_Common.Entities
{
	public class Item
	{

		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Producer { get; set; }
		public string Type { get; set; }
		public double Price { get; set; }
		public string ListingDate { get; set; }
		public Guid AddedById { get; set; }

	}
}
