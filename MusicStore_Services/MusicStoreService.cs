﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MusicStore_Common.DTOs;
using MusicStore_Common.Util;
using MusicStore_Interfaces;

namespace MusicStore_Services
{
	public class MusicStoreService : IMusicStoreService
	{

		private readonly IMusicStoreRepository _musicStoreRepository;

		public MusicStoreService(IMusicStoreRepository musicStoreRepository)
		{
			_musicStoreRepository = musicStoreRepository;
		}

		/// <summary>
		/// Add the new item to the collection.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public ItemDto Add(ItemDto itemDto)
		{
			var item = itemDto.ToEntity();
			var addedItem = _musicStoreRepository.Add(item);
			return addedItem.ToDto();
		}

		/// <summary>
		/// Get the collection of items.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<ItemDto> Get()
		{
			var itemsList = _musicStoreRepository.Get();
			return itemsList.Any() ? itemsList.Select(x => x.ToDto()) : new List<ItemDto>();
		}

		/// <summary>
		/// Get the collection of items, in a paged format.
		/// </summary>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public IEnumerable<ItemDto> Get(int pageNumber = 1, int pageSize = 100)
		{
			var results = _musicStoreRepository.Get(pageNumber, pageSize).ToList();
			return results.Any() ? results.Select(x => x.ToDto()) : new List<ItemDto>();
		}

		/// <summary>
		/// Get the item by the given id.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public ItemDto GetById(Guid id)
		{
			return _musicStoreRepository.GetById(id)?.ToDto();
		}

		/// <summary>
		/// Remove the item with the given id from the collection.
		/// Throws KeyNotFoundException if the entity cannot be found.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public bool Remove(Guid id)
		{
			return _musicStoreRepository.Remove(id);
		}

		/// <summary>
		/// Update the item animal with the new values.
		/// Throws KeyNotFoundException if the entity cannot be found.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool Update(ItemDto item)
		{
			return _musicStoreRepository.Update(item.ToEntity());
		}
	}
}
